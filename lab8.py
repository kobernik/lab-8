#Подключение библиотек
import math
import random
from timeit import default_timer as timer

#Функция заполнения массивов значениями координат
def mas_random_points(var):
	mas_points = []
	for j in range(var):
		x = random.uniform(bord_1, bord_2)
		y = random.uniform(bord_1, bord_2)
		mas_points.append((x, y))
	return mas_points

#Функция подсчета кол-ва точек входящих в границы радиуса
def selection_points(var, c_p, mas_points, r):
	summ = 0
	for k in range(var):
		if mas_points[c_p][0] != mas_points[k][0] and math.sqrt((mas_points[c_p][0] - mas_points[k][0]) ** 2 + (mas_points[c_p][1] - mas_points[k][1]) ** 2) <= r:
			summ += 1
	return summ

while True:
	#Проверка вводимых данных
	try:
		#Решение пользователя: запустить подсчет времени вывода точек или нет
		answer = int(input('Запустить подсчет времени вывода точек от 100000 до 1000000 (с шагом в 100000)?\n1 - "ДА"\n0 - "НЕТ, хочу вводить свои значения"\nВаш ответ: '))
		print()
		if answer < 0 or answer > 1:
			print('Ответьте "0" или "1"!\n')
		else:
			break
	except ValueError:
		print('\nВведен неверный формат данных!\n')

while True:
	#Проверка вводимых данных
	try:
		#Ввод границ генерирования значений координат
		print('Введите границы генерирования значений координат:')
		bord_1 = int(input('Граница 1 = '))
		bord_2 = int(input('Граница 2 = '))
		print()
		if bord_1 > bord_2:
			print('Значение первой границы должно быть меньше значения второй границы!\n')
		else:
			break
	except ValueError:
		print('\nВведен неверный формат данных!\n')

if math.isclose(answer, 0, abs_tol = 0.01):
	while True:
		#Проверка вводимых данных
		try:
			#Ввод кол-ва точек на координатной плоскости
			qnt_points = int(input('Введите кол-во точек на координатной плоскости: '))
			print()
			if qnt_points <= 0:
				print('Кол-во точек должно быть больше нуля!\n')
			else:
				break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	points = mas_random_points(qnt_points)

	while True:
		#Проверка вводимых данных
		try:
			#Выбор точки
			num_point = int(input('Выберите любую точку: '))
			print()
			if num_point < 1 or num_point > qnt_points:
				print('Такой точки нет. Выберите точку от 1 до', qnt_points, '!\n')
			else:
				center_point = num_point - 1
				break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	while True:
		#Проверка вводимых данных
		try:
			#Ввод значения радиуса
			radius = int(input('Введите радиус границ окрестности выбранной точки: '))
			if radius < 0:
				print('\nРадиус должен быть больше нуля или равен нулю!\n')
			else:
				break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	n = selection_points(qnt_points, center_point, points, radius)
	print('\nИз', qnt_points,'точек в радиус входит', n,'точек(ки).')

else:
	file = open('time_file.txt', 'w')
	#Отбор точек входящих в границы радиуса
	for i in range(100000, 1000001, 100000):
		#Ввод данных массивов
		avg_val = 0

		radius = random.randint(bord_1, bord_2)

		center_point = random.randint(1, i)

		points = mas_random_points(i)

		for _ in range(3):
			start_time = timer()
			n = selection_points(i, center_point, points, radius)
			avg_val += timer() - start_time

		print('Из', i,'точек в радиус входит', n,'точек(ки).')

		file.write(str(i) + ' ' + str(avg_val / 3).replace('.', ',') + '\n')
	file.close()
